<?php
/**
* Template Name: Filter Folio 2 cols
*/

get_header(); ?>
<header class="motopress-wrapper header">
    <div class="container">
        <div class="row">
            <div class="span12" data-motopress-wrapper-file="wrapper/wrapper-header.php" data-motopress-wrapper-type="header">
                <?php get_template_part('wrapper/wrapper-header'); ?>
            </div>
        </div>
    </div>
</header>

<div class="motopress-wrapper content-holder clearfix">
    <div class="container">
        <div class="row">
            <div class="span12" data-motopress-wrapper-file="page-Portfolio2Cols-filterable.php" data-motopress-wrapper-type="content">
                <div class="row">
                    <div class="span12" data-motopress-type="static" data-motopress-static-file="static/static-title.php">
                        <?php get_template_part("static/static-title"); ?>
                    </div>
                </div>
                <div id="content" class="row">
                    <div class="span12" data-motopress-type="loop" data-motopress-loop-file="loop/loop-portfolio2.php">
                        <?php get_template_part("loop/loop-portfolio2"); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<footer class="motopress-wrapper footer">
    <div class="container">
        <div class="row">
            <div class="span12" data-motopress-wrapper-file="wrapper/wrapper-footer.php" data-motopress-wrapper-type="footer">
                <?php get_template_part('wrapper/wrapper-footer'); ?>
            </div>
        </div>
    </div>
</footer>
<?php get_footer(); ?>