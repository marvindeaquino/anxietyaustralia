<?php
// Do not delete these lines
	if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die ('Please do not load this page directly. Thanks!');

	if ( post_password_required() ) { ?>
  	<?php echo '<p class="nocomments">' . __('This post is password protected. Enter the password to view comments.', CURRENT_THEME ) . '</p>'; ?>
	<?php
		return;
	}
?>
<!-- BEGIN Comments -->	
	<?php if ( have_comments() ) : ?>
	<div class="comment-holder">
		<h3 class="comments-h"><?php printf( _n( '1 Comment', '%1$s Comments', get_comments_number(), CURRENT_THEME ),
				number_format_i18n( get_comments_number() ), '' );?></h3>
		<div class="pagination">
		  <?php paginate_comments_links('prev_text=Prev&next_text=Next'); ?> 
		</div>
		<ol class="comment-list clearfix">
			<?php wp_list_comments('type=all&callback=mytheme_comment'); ?>
		</ol>
		<div class="pagination">
		  <?php paginate_comments_links('prev_text=Prev&next_text=Next'); ?> 
		</div>
	</div>
	<?php else : // this is displayed if there are no comments so far ?>

	<?php if ( comments_open() ) : ?>
			<!-- If comments are open, but there are no comments. -->
	   <?php echo '<p class="nocomments">' . __('No Comments Yet.', CURRENT_THEME ) . '</p>'; ?>
		<?php else : // comments are closed ?>
			<!-- If comments are closed. -->
	   <?php echo '<p class="nocomments">' . __('Comments are closed.', CURRENT_THEME ) . '</p>'; ?>

		<?php endif; ?>
	
	<?php endif; ?>
	

	<?php if ( comments_open() ) : ?>

	<div id="respond">

	<h3><?php comment_form_title( _e('Leave a comment', CURRENT_THEME )); ?></h3>

	<div class="cancel-comment-reply">
		<small><?php cancel_comment_reply_link(); ?></small>
	</div>

	<?php if ( get_option('comment_registration') && !is_user_logged_in() ) : ?>
	<p><?php _e('You must be', CURRENT_THEME ); ?> <a href="<?php echo wp_login_url( get_permalink() ); ?>"><?php _e('logged in', CURRENT_THEME ); ?></a> <?php _e('to post a comment.', CURRENT_THEME ); ?></p>
	<?php else : ?>

	<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">

	<?php if ( is_user_logged_in() ) : ?>

	<p><?php _e('Logged in as', CURRENT_THEME ); ?> <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="<?php _e('Log out of this account', CURRENT_THEME ); ?>"><?php _e('Log out &raquo;', CURRENT_THEME ); ?></a></p>

	<?php else : ?>

	<p class="field"><input type="text" name="author" id="author" value="<?php _e('Name', CURRENT_THEME); ?><?php if ($req) _e('*', CURRENT_THEME); ?>" onfocus="if(this.value=='<?php _e('Name', CURRENT_THEME); ?><?php if ($req) _e('*', CURRENT_THEME); ?>'){this.value=''}" onblur="if(this.value==''){this.value='<?php _e('Name', CURRENT_THEME); ?><?php if ($req) _e('*', CURRENT_THEME); ?>'}" size="22" tabindex="1" <?php if ($req) echo "aria-required='true'"; ?> /></p>

	<p class="field"><input type="text" name="email" id="email" value="<?php _e('Email (will not be published)', CURRENT_THEME); ?><?php if ($req) _e('*', CURRENT_THEME); ?>" onfocus="if(this.value=='<?php _e('Email (will not be published)', CURRENT_THEME); ?><?php if ($req) _e('*', CURRENT_THEME); ?>'){this.value=''}" onblur="if(this.value==''){this.value='<?php _e('Email (will not be published)', CURRENT_THEME); ?><?php if ($req) _e('*', CURRENT_THEME); ?>'}" size="22" tabindex="2" <?php if ($req) echo "aria-required='true'"; ?> /></p>

	<p class="field"><input type="text" name="url" id="url" value="<?php _e('Website', CURRENT_THEME); ?>" onfocus="if(this.value=='<?php _e('Website', CURRENT_THEME); ?>'){this.value=''}" onblur="if(this.value==''){this.value='<?php _e('Website', CURRENT_THEME); ?>'}" size="22" tabindex="3" /></p>

	<?php endif; ?>

	<!-- <p>You may use these <abbr title="HyperText Markup Language">HTML</abbr> tags and attributes: <code><?php echo allowed_tags(); ?></code></small></p> -->

	<p><textarea name="comment" id="comment" cols="58" rows="10" tabindex="4" onfocus="if(this.value=='<?php _e('Your comment*', CURRENT_THEME); ?>'){this.value=''}" onblur="if(this.value==''){this.value='<?php _e('Your comment*', CURRENT_THEME); ?>'}"><?php _e('Your comment*', CURRENT_THEME); ?></textarea></p>

	<p><input name="submit" type="submit" class="btn btn-primary" id="submit" tabindex="5" value="<?php _e('Submit Comment', CURRENT_THEME); ?>" />
		<?php comment_id_fields(); ?>
	</p>
	<?php do_action('comment_form', $post->ID); ?>

	</form>

	<?php endif; // If registration required and not logged in ?>
	</div>

<!-- END Comments -->

<?php endif; ?>