<?php get_header(); ?>
<header class="motopress-wrapper header">
    <div class="container">
        <div class="row">
            <div class="span12" data-motopress-wrapper-file="wrapper/wrapper-header.php" data-motopress-wrapper-type="header">
                <?php get_template_part('wrapper/wrapper-header'); ?>
            </div>
        </div>
    </div>
</header>

<div class="motopress-wrapper content-holder clearfix">
    <div class="container">
        <div class="row">
            <div class="span12" data-motopress-wrapper-file="404.php" data-motopress-wrapper-type="content">
                <div class="row error404-holder">
                    <div class="span7 error404-holder_num" data-motopress-type="static" data-motopress-static-file="static/static-404.php">
                    	<?php get_template_part("static/static-404"); ?>
                    </div>
                    <div class="span5" data-motopress-type="static" data-motopress-static-file="static/static-not-found.php">
                    	<?php get_template_part("static/static-not-found"); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<footer class="motopress-wrapper footer">
    <div class="container">
        <div class="row">
            <div class="span12" data-motopress-wrapper-file="wrapper/wrapper-footer.php" data-motopress-wrapper-type="footer">
                <?php get_template_part('wrapper/wrapper-footer'); ?>
            </div>
        </div>
    </div>
</footer>
<?php get_footer(); ?>