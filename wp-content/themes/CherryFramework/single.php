<?php get_header(); ?>
<header class="motopress-wrapper header">
    <div class="container">
        <div class="row">
            <div class="span12" data-motopress-wrapper-file="wrapper/wrapper-header.php" data-motopress-wrapper-type="header">
                <?php get_template_part('wrapper/wrapper-header'); ?>
            </div>
        </div>
    </div>
</header>

<div class="motopress-wrapper content-holder clearfix">
    <div class="container">
        <div class="row">
            <div class="span12" data-motopress-wrapper-file="single.php" data-motopress-wrapper-type="content">
                <div class="row">
                    <div class="span8 <?php echo of_get_option('blog_sidebar_pos'); ?>" id="content" data-motopress-type="loop" data-motopress-loop-file="loop/loop-single.php">
                        <?php get_template_part("loop/loop-single"); ?>
                    </div>
                    <div class="span4 sidebar" id="sidebar" data-motopress-type="dynamic-sidebar" data-motopress-sidebar-id="main-sidebar">
                        <?php dynamic_sidebar("main-sidebar"); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<footer class="motopress-wrapper footer">
    <div class="container">
        <div class="row">
            <div class="span12" data-motopress-wrapper-file="wrapper/wrapper-footer.php" data-motopress-wrapper-type="footer">
                <?php get_template_part('wrapper/wrapper-footer'); ?>
            </div>
        </div>
    </div>
</footer>
<?php get_footer(); ?>