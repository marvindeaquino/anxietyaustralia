<?php // Theme Options vars
$folio_filter = of_get_option('folio_filter');

switch ($folio_filter) {
	case 'cat': ?>
		<div class="filter-wrapper clearfix">
			<div class="pull-right">
				<strong>Categories: </strong>
				<ul id="filters" class="filter nav nav-pills" data-filter-group="category">
					<li class="active"><a href="#" data-filter><?php echo __('Show all', CURRENT_THEME); ?></a></li>
					<?php 
						$portfolio_categories = get_categories(array('taxonomy'=>'portfolio_category'));
						$filter_array = array();
						$the_query = new WP_Query();
						if ($paged == 0)
							$paged = 1;
						$custom_count = ($paged - 1) * $items_count;
						$the_query->query('post_type=portfolio&showposts='. $items_count .'&offset=' . $custom_count);
						while( $the_query->have_posts() ) :
							$the_query->the_post();
							$post_id = $the_query->post->ID;
							$terms = get_the_terms( $post_id, 'portfolio_category');
							if ( $terms && ! is_wp_error( $terms ) ) {
								foreach ( $terms as $term )
									$filter_array[$term->slug] = $term;
							}
						endwhile;
						foreach ($filter_array as $key => $value)
							echo '<li><a href="#" data-filter=".'.$key.'">' . $value->name . '</a></li>';
						wp_reset_postdata();
					?>
				</ul>
				<div class="clear"></div>
			</div>
		</div>
		<?php
		break;
	case 'tag': ?>
		<div class="filter-wrapper clearfix">
			<div class="pull-right">
				<strong>Tags: </strong>
				<ul id="tags" class="filter nav nav-pills" data-filter-group="tag">
					<li class="active"><a href="#" data-filter><?php echo __('Show all', CURRENT_THEME); ?></a></li>
					<?php 
						$portfolio_tags = get_terms('portfolio_tag');
						$filter_array = array();
						$the_query = new WP_Query();
						if ($paged == 0) {
							$paged = 1;
						}
						$custom_count = ($paged - 1) * $items_count;
						$the_query->query('post_type=portfolio&showposts='. $items_count .'&offset=' . $custom_count);
						while( $the_query->have_posts() ) :
							$the_query->the_post();
							$post_id = $the_query->post->ID;
							$terms = get_the_terms( $post_id, 'portfolio_tag');
							if ( $terms && ! is_wp_error( $terms ) ) {
								foreach ( $terms as $term )
									$filter_array[$term->slug] = $term;
							}
						endwhile;
						foreach ($filter_array as $key => $value)
							echo '<li><a href="#" data-filter=".'.$key.'">' . $value->name . '</a></li>';
						wp_reset_postdata();
					?>
				</ul>
				<div class="clear"></div>
			</div>
		</div>
		<?php
		break;
	case 'both': ?>
		<div class="filter-wrapper clearfix">
			<div class="pull-right">
				<strong>Categories: </strong>
				<ul id="filters" class="filter nav nav-pills" data-filter-group="category">
					<li class="active"><a href="#" data-filter><?php echo __('Show all', CURRENT_THEME); ?></a></li>
					<?php 
						$portfolio_categories = get_categories(array('taxonomy'=>'portfolio_category'));
						$filter_array = array();
						$the_query = new WP_Query();
						if ($paged == 0)
							$paged = 1;
						$custom_count = ($paged - 1) * $items_count;
						$the_query->query('post_type=portfolio&showposts='. $items_count .'&offset=' . $custom_count);
						while( $the_query->have_posts() ) :
							$the_query->the_post();
							$post_id = $the_query->post->ID;
							$terms = get_the_terms( $post_id, 'portfolio_category');
							if ( $terms && ! is_wp_error( $terms ) ) {
								foreach ( $terms as $term )
									$filter_array[$term->slug] = $term;
							}
						endwhile;
						foreach ($filter_array as $key => $value)
							echo '<li><a href="#" data-filter=".'.$key.'">' . $value->name . '</a></li>';
						wp_reset_postdata();
					?>
				</ul>
				<div class="clear"></div>
				<strong>Tags: </strong>
				<ul id="tags" class="filter nav nav-pills" data-filter-group="tag">
					<li class="active"><a href="#" data-filter><?php echo __('Show all', CURRENT_THEME); ?></a></li>
					<?php 
						$portfolio_tags = get_terms('portfolio_tag');
						$filter_array = array();
						$the_query = new WP_Query();
						if ($paged == 0) {
							$paged = 1;
						}
						$custom_count = ($paged - 1) * $items_count;
						$the_query->query('post_type=portfolio&showposts='. $items_count .'&offset=' . $custom_count);
						while( $the_query->have_posts() ) :
							$the_query->the_post();
							$post_id = $the_query->post->ID;
							$terms = get_the_terms( $post_id, 'portfolio_tag');
							if ( $terms && ! is_wp_error( $terms ) ) {
								foreach ( $terms as $term )
									$filter_array[$term->slug] = $term;
							}
						endwhile;
						foreach ($filter_array as $key => $value)
							echo '<li><a href="#" data-filter=".'.$key.'">' . $value->name . '</a></li>';
						wp_reset_postdata();
					?>
				</ul>
				<div class="clear"></div>
			</div>
		</div>
		<?php
		break;
	default:
		break;
}?>

<?php 
	$temp = $wp_query;
	$wp_query= null;
	$wp_query = new WP_Query();
	$wp_query->query("post_type=portfolio&paged=".$paged.'&showposts='.$items_count); 
?>
	
<?php if ( ! have_posts() ) : ?>
	<div id="post-0" class="post error404 not-found">
		<h1 class="entry-title"><?php _e( 'Not Found', CURRENT_THEME ); ?></h1>
		<div class="entry-content">
			<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', CURRENT_THEME ); ?></p>
			<?php get_search_form(); ?>
		</div><!-- .entry-content -->
	</div><!-- #post-0 -->
<?php endif; ?>

<ul id="portfolio-grid" class="filterable-portfolio thumbnails portfolio-<?php echo $cols; ?>" data-cols="<?php echo $cols; ?>">
	<?php get_template_part('filterable-portfolio-loop'); ?>
</ul>

<?php 
	get_template_part('includes/post-formats/post-nav');
	$wp_query = null;
	$wp_query = $temp;
?>