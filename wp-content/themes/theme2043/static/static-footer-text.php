<?php /* Static Name: Footer text */ ?>
<div id="footer-text" class="footer-text">
	<?php $myfooter_text = of_get_option('footer_text'); ?>
	
	<?php if($myfooter_text){?>
		<?php echo of_get_option('footer_text'); ?>
	<?php } else { ?>
		
		
		<?php if(of_get_option('f_logo_type') == 'f_text_logo'){?>
				<a href="<?php echo home_url(); ?>/" title="<?php bloginfo('description'); ?>" class="site-name"><?php bloginfo('name'); ?></a>
		<?php } else { ?>
				<?php if(of_get_option('f_logo_url') == ''){ ?>
						<a href="<?php echo home_url(); ?>/" class="logo_f"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/footer-logo.png" alt="<?php bloginfo('name'); ?>" title="<?php bloginfo('description'); ?>"></a>
				<?php } else  { ?>
						<a href="<?php echo home_url(); ?>/" class="logo_f"><img src="<?php echo of_get_option('f_logo_url', "" ); ?>" alt="<?php bloginfo('name'); ?>" title="<?php bloginfo('description'); ?>"></a>
				<?php }?>
		<?php }?>
		
		
		
		
	<?php } ?>
	<br>
	<?php if( is_front_page() ) { ?>
		<!-- {%FOOTER_LINK} -->
	<?php } ?>
</div>
