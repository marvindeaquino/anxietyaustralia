<?php /* Wrapper Name: Header */ ?>
<div class="row">
	<div id="header-row" class="span12" style="border:0px solid #000000;">
	    <div style="border:0px solid #000000;" class="span4" data-motopress-type="static" data-motopress-static-file="static/static-booking.php">
	    	<?php get_template_part("static/static-booking"); ?>
	    </div>
	    <div style="border:0px solid #000000;" class="span4" data-motopress-type="static" data-motopress-static-file="static/static-logo.php">
	    	<?php get_template_part("static/static-logo"); ?>
	    </div>
	    <div style="border:0px solid #000000;" class="span4" data-motopress-type="dynamic-sidebar" data-motopress-sidebar-id="header-area">
	        <?php dynamic_sidebar("header-area"); ?>
	    </div>
	</div>
</div>
<div class="row">
    <div class="span12" data-motopress-type="static" data-motopress-static-file="static/static-nav.php">
    	<?php get_template_part("static/static-nav"); ?>
    </div>
</div>
