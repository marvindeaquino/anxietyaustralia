<?php /* Wrapper Name: Footer */ ?>
<div class="row">
    <div class="span3 copyright" data-motopress-type="static" data-motopress-static-file="static/static-footer-text.php">
    	<?php get_template_part("static/static-footer-text"); ?>
    </div>
    <div class="span9">
	<div class="row footer-widgets">
	    <div class="span2" data-motopress-type="dynamic-sidebar" data-motopress-sidebar-id="footer-sidebar-1">
		<?php dynamic_sidebar("footer-sidebar-1"); ?>
	    </div>
	    <div class="span2" data-motopress-type="dynamic-sidebar" data-motopress-sidebar-id="footer-sidebar-2">
		<?php dynamic_sidebar("footer-sidebar-2"); ?>
	    </div>
	    <div class="span2" data-motopress-type="dynamic-sidebar" data-motopress-sidebar-id="footer-sidebar-3">
		<?php dynamic_sidebar("footer-sidebar-3"); ?>
	    </div>
	    <div class="span3" data-motopress-type="dynamic-sidebar" data-motopress-sidebar-id="footer-sidebar-4">
		<?php dynamic_sidebar("footer-sidebar-4"); ?>
	    </div>	
	</div>
    </div>
</div>
<div class="row">
    <div class="span12" data-motopress-type="static" data-motopress-static-file="static/static-footer-nav.php">
    	<?php get_template_part("static/static-footer-nav"); ?>
    </div>
</div>

<div><span class="copyright_text_left" style="font-size:12px">&copy; <?php echo date('Y'); ?> Anxiety Treatment Australia ABN 51 109 368 630</span><a href="<?php echo home_url(); ?>/privacy-policy/" title="Privacy Policy"><?php //_e('Privacy Policy', CURRENT_THEME); ?></a><span class="copyright_text_right" style="font-size:12px;float: right;
">Website Designed By <a href="#" style="color:#1F74BA">Light Media</a></span></div>