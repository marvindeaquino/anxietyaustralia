<?php
	@define( 'CURRENT_THEME', 'themeXXXX');

	// Loading child theme textdomain
	load_child_theme_textdomain( CURRENT_THEME, get_stylesheet_directory() . '/languages' );

	// WP Pointers
	add_action('admin_enqueue_scripts', 'myHelpPointers');
	function myHelpPointers() {
	//First we define our pointers 
	$pointers = array(
	   	array(
	       'id' => 'xyz1',   // unique id for this pointer
	       'screen' => 'options-permalink', // this is the page hook we want our pointer to show on
	       'target' => '#submit', // the css selector for the pointer to be tied to, best to use ID's
	       'title' => 'Submit Permalink Structure',
	       'content' => 'This way of links configuration can be used by not only our blog followers but will help in SEO-optimisation as well. The effectiveness and main features of this link configuration method are revealed <a href="http://codex.wordpress.org/Using_Permalinks">here</a>',
	       'position' => array( 
	                          'edge' => 'top', //top, bottom, left, right
	                          'align' => 'left', //top, bottom, left, right, middle
	                          'offset' => '0 5'
	                          )
	       ),

	    array(
	       'id' => 'xyz2',   // unique id for this pointer
	       'screen' => 'themes', // this is the page hook we want our pointer to show on
	       'target' => '#toplevel_page_options-framework', // the css selector for the pointer to be tied to, best to use ID's
	       'title' => 'Import Sample Data',
	       'content' => 'If you want to install sample data from livedemo you need to go to <strong>Cherry Options</strong> > <strong>Import</strong> and follow the tips.',
	       'position' => array( 
	                          'edge' => 'bottom', //top, bottom, left, right
	                          'align' => 'top', //top, bottom, left, right, middle
	                          'offset' => '0 -10'
	                          )
	       ),

	    array(
	       'id' => 'xyz3',   // unique id for this pointer
	       'screen' => 'toplevel_page_options-framework', // this is the page hook we want our pointer to show on
	       'target' => '#toplevel_page_options-framework', // the css selector for the pointer to be tied to, best to use ID's
	       'title' => 'Import Sample Data',
	       'content' => 'If you want to install sample data from livedemo you need to go to <strong>Import</strong> and follow the tips.',
	       'position' => array( 
	                          'edge' => 'left', //top, bottom, left, right
	                          'align' => 'top', //top, bottom, left, right, middle
	                          'offset' => '0 18'
	                          )
	       )
	    // more as needed
	    );
		//Now we instantiate the class and pass our pointer array to the constructor 
		$myPointers = new WP_Help_Pointer($pointers); 
	}
	
	/**
 * Service Box
 *
 */
if (!function_exists('service_box_shortcode')) {

	function service_box_shortcode($atts, $content = null) { 
	    extract(shortcode_atts(
	        array(
				'title' => '',
				'subtitle' => '',
				'icon' => '',
				'text' => '',
				'btn_text' => 'read more',
				'btn_link' => '',
				'btn_size' => '',
				'target' => '',
				'custom_class' => ''
	    ), $atts));
		
		$template_url = get_stylesheet_directory_uri();
	 
		$output =  '<div class="service-box '.$custom_class.'">';
		
		// check what icon user selected
		switch ($icon) {
			case 'no':
				break;
	       	case 'icon1':
				$output .= '<figure class="icon"><img src="' .$template_url. '/images/'. $icon .'.png" alt="" /></figure>';
				break;
	       	case 'icon2':
				$output .= '<figure class="icon"><img src="' .$template_url. '/images/'. $icon .'.png" alt="" /></figure>';
				break;
			case 'icon3':
				$output .= '<figure class="icon"><img src="' .$template_url. '/images/'. $icon .'.png" alt="" /></figure>';
				break;
			case 'icon4':
				$output .= '<figure class="icon"><img src="' .$template_url. '/images/'. $icon .'.png" alt="" /></figure>';
				break;
			case 'icon5':
				$output .= '<figure class="icon"><img src="' .$template_url. '/images/'. $icon .'.png" alt="" /></figure>';
				break;
			case 'icon6':
				$output .= '<figure class="icon"><img src="' .$template_url. '/images/'. $icon .'.png" alt="" /></figure>';
				break;
			case 'icon7':
				$output .= '<figure class="icon"><img src="' .$template_url. '/images/'. $icon .'.png" alt="" /></figure>';
				break;
			case 'icon8':
				$output .= '<figure class="icon"><img src="' .$template_url. '/images/'. $icon .'.png" alt="" /></figure>';
				break;
			case 'icon9':
				$output .= '<figure class="icon"><img src="' .$template_url. '/images/'. $icon .'.png" alt="" /></figure>';
				break;
			case 'icon10':
				$output .= '<figure class="icon"><img src="' .$template_url. '/images/'. $icon .'.png" alt="" /></figure>';
				break;
	    }

	   $output .= '<div class="service-box_body">';
	 
		if ($title!="") {
			$output .= '<h2 class="title">';
			$output .= $title;
			$output .= '</h2>';
		}	 
		if ($subtitle!="") {
			$output .= '<h5 class="sub-title">';
			$output .= $subtitle;
			$output .= '</h5>';
		}		
		if ($text!="") {
			$output .= '<div class="service-box_txt">';
			$output .= $text;
			$output .= '</div>';
		}		
		if ($btn_link!="") {	
			$output .=  '<div class="btn-align"><a href="'.$btn_link.'" title="'.$btn_text.'" class="btn btn-link btn-'.$btn_size.'" target="'.$target.'">';
			$output .= $btn_text;
			$output .= '</a></div>';
		}
		$output .= '</div>';	 
		$output .= '</div><!-- /Service Box -->';	 
	    return $output;	 
	} 
	add_shortcode('service_box', 'service_box_shortcode');

}

// Fullwidth Horizontal Rule
function fullwidth_hr_shortcode($atts, $content = null) {

    $output = '<div class="hr fullwidth">';
        $output .= do_shortcode($content);
    $output .= '</div>';

    return $output;
}
add_shortcode('fullwidth_hr', 'fullwidth_hr_shortcode');

// Fullwidth Box
function fullwidth_box_shortcode($atts, $content = null) {

    $output = '<div class="fullwidth-box">';
        $output .= do_shortcode($content);
    $output .= '</div>';

    return $output;
}
add_shortcode('fullwidth_box', 'fullwidth_box_shortcode');

/*-----------------------------------------------------------------------------------*/
/* Custom Comments Structure
/*-----------------------------------------------------------------------------------*/
if ( !function_exists( 'mytheme_comment' ) ) {
	function mytheme_comment($comment, $args, $depth) {
	     $GLOBALS['comment'] = $comment;

	?> 
	   <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>" class="clearfix">
	     	<div id="comment-<?php comment_ID(); ?>" class="comment-body clearfix">
	      		<div class="wrapper">
	      			<div class="comment-author vcard">
	  	         		<?php echo get_avatar( $comment->comment_author_email, 80 ); ?>
	  	  				<?php printf(__('<span class="author">%1$s</span>' ), get_comment_author_link()) ?>
	  	      		</div>
	  		      	<?php if ($comment->comment_approved == '0') : ?>
	  		        	<em><?php _e('Your comment is awaiting moderation.', CURRENT_THEME) ?></em>
	  		      	<?php endif; ?>	      	
	  		     	<div class="extra-wrap">
	  		     		<?php comment_text() ?>	     	
	  		     	</div>
	  		    </div>
		     	<div class="wrapper">
				  	<div class="reply">
				    	<?php comment_reply_link(array_merge( $args, array('reply_text' => 'comment', 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
				   	</div>
			   		<div class="comment-meta commentmetadata"><?php printf(__('%1$s', CURRENT_THEME ), get_comment_date('F j, Y')) ?></div>
			 	</div>
	    	</div>
	<?php }
}

/**
 * Mini Post Grid
 *
 */
if (!function_exists('mini_posts_grid_shortcode')) {

	function mini_posts_grid_shortcode($atts, $content = null) {
		extract(shortcode_atts(array(
			'type' => '',
			'numb' => '8',
			'thumbs' => '',
			'thumb_width' => '',
			'thumb_height' => '',
			'order_by' => 'date',
			'order' => 'DESC',
			'align' => '',
			'custom_class' => ''
		), $atts));

		$template_url = get_stylesheet_directory_uri();

		// check what order by method user selected
		switch ($order_by) {
			case 'date':
				$order_by = 'post_date';
				break;
			case 'title':
				$order_by = 'title';
				break;
			case 'popular':
				$order_by = 'comment_count';
				break;
			case 'random':
				$order_by = 'rand';
				break;
		}

		// check what order method user selected (DESC or ASC)
		switch ($order) {
			case 'DESC':
				$order = 'DESC';
				break;
			case 'ASC':
				$order = 'ASC';
				break;
		}

		// thumbnail size
		$thumb_x = 0;
		$thumb_y = 0;
		if (($thumb_width != '') && ($thumb_height != '')) {
			$thumbs = 'custom_thumb';
			$thumb_x = $thumb_width;
			$thumb_y = $thumb_height;
		} else {
			switch ($thumbs) {
				case 'small':
					$thumb_x = 110;
					$thumb_y = 110;
					break;
				case 'smaller':
					$thumb_x = 90;
					$thumb_y = 90;
					break;
				case 'smallest':
					$thumb_x = 60;
					$thumb_y = 60;
					break;
			}
		}	

			global $post;
			global $my_string_limit_words;
							
			$args = array(
				'post_type' => $type,
				'numberposts' => $numb,
				'orderby' => $order_by,
				'order' => $order
			);		

			$posts = get_posts($args);
			$i = 0;

			$output = '<ul class="mini-posts-grid grid-align-'.$align.' unstyled '.$custom_class.'">';
			
			foreach($posts as $post) {
				setup_postdata($post);
				$excerpt = get_the_excerpt();
				$attachment_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
				$url = $attachment_url['0'];
				$image = aq_resize($url, $thumb_x, $thumb_y, true);
				$mediaType = get_post_meta($post->ID, 'tz_portfolio_type', true);
				$prettyType = 0;

					$output .= '<li class="'.$thumbs.'">';
						if(has_post_thumbnail($post->ID) && $mediaType == 'Image') {
												
							$prettyType = 'prettyPhoto';									

							$output .= '<figure class="featured-thumbnail thumbnail">';
							$output .= '<a href="'.$url.'" title="'.get_the_title($post->ID).'" rel="' .$prettyType.'">';
							$output .= '<img  src="'.$image.'" alt="'.get_the_title($post->ID).'" />';
							$output .= '<span class="zoom-icon"><span class="zoom-text">'.__('zoom', CURRENT_THEME).'</span></span></a></figure>';
						} elseif ($mediaType != 'Video' && $mediaType != 'Audio') {							

							$thumbid = 0;
							$thumbid = get_post_thumbnail_id($post->ID);
											
							$images = get_children( array(
								'orderby' => 'menu_order',
								'order' => 'ASC',
								'post_type' => 'attachment',
								'post_parent' => $post->ID,
								'post_mime_type' => 'image',
								'post_status' => null,
								'numberposts' => -1
							) ); 

							if ( $images ) {

								$k = 0;
								//looping through the images
								foreach ( $images as $attachment_id => $attachment ) {
									$prettyType = "prettyPhoto[gallery".$i."]";								
									//if( $attachment->ID == $thumbid ) continue;

									$image_attributes = wp_get_attachment_image_src( $attachment_id, 'full' ); // returns an array
									$img = aq_resize( $image_attributes[0], $thumb_x, $thumb_y, true ); //resize & crop img
									$alt = get_post_meta($attachment->ID, '_wp_attachment_image_alt', true);
									$image_title = $attachment->post_title;
									
									if ( $k == 0 ) {
										if (has_post_thumbnail($post->ID)) {
											$output .= '<figure class="featured-thumbnail thumbnail">';
											$output .= '<a href="'.$image_attributes[0].'" title="'.get_the_title($post->ID).'" rel="' .$prettyType.'">';
											$output .= '<img src="'.$image.'" alt="'.get_the_title($post->ID).'" />';
										} else {
											$output .= '<figure class="featured-thumbnail thumbnail">';
											$output .= '<a href="'.$image_attributes[0].'" title="'.get_the_title($post->ID).'" rel="' .$prettyType.'">';
											$output .= '<img  src="'.$img.'" alt="'.get_the_title($post->ID).'" />';
										}	
									} else {
										$output .= '<figure class="featured-thumbnail thumbnail" style="display:none;">';
										$output .= '<a href="'.$image_attributes[0].'" title="'.get_the_title($post->ID).'" rel="' .$prettyType.'">';
										$output .= '<img  src="'.$img.'" alt="'.get_the_title($post->ID).'" />';
									}
									$output .= '<span class="zoom-icon"><span class="zoom-text">'.__('zoom', CURRENT_THEME).'</span></span></a></figure>';
									$k++;
								}					
							} elseif (has_post_thumbnail($post->ID)) {
								$prettyType = 'prettyPhoto';
								$output .= '<figure class="featured-thumbnail thumbnail">';
								$output .= '<a href="'.$url.'" title="'.get_the_title($post->ID).'" rel="' .$prettyType.'">';
								$output .= '<img  src="'.$image.'" alt="'.get_the_title($post->ID).'" />';
								$output .= '<span class="zoom-icon"><span class="zoom-text">'.__('zoom', CURRENT_THEME).'</span></span></a></figure>';
							} else {
								// empty_featured_thumb.gif - for post without featured thumbnail
								$output .= '<figure class="featured-thumbnail thumbnail">';
								$output .= '<a href="'.get_permalink($post->ID).'" title="'.get_the_title($post->ID).'">';
								$output .= '<img  src="'.$template_url.'/images/empty_thumb.gif" alt="'.get_the_title($post->ID).'" />';
								$output .= '</a></figure>';
							}
						} else {

							// for Video and Audio post format - no lightbox
							$output .= '<figure class="featured-thumbnail thumbnail"><a href="'.get_permalink($post->ID).'" title="'.get_the_title($post->ID).'">';
							$output .= '<img  src="'.$image.'" alt="'.get_the_title($post->ID).'" />';
							$output .= '</a></figure>';
						}

						$output .= '</li>';
				$i++;		

			} // end foreach
			$output .= '</ul><!-- .posts-grid (end) -->';		
		$output .= '<div class="clear"></div>';
		return $output;
	} 
	add_shortcode('mini_posts_grid', 'mini_posts_grid_shortcode');
	
}

/**
 * Post Grid
 *
 */
if (!function_exists('posts_grid_shortcode')) {

	function posts_grid_shortcode($atts, $content = null) {
		extract(shortcode_atts(array(
			'type' => '',
			'columns' => '3',
			'rows' => '3',
			'order_by' => 'date',
			'order' => 'DESC',
			'thumb_width' => '370',
			'thumb_height' => '250',
			'meta' => '',
			'excerpt_count' => '15',
			'link' => 'yes',
			'link_text' => 'read more',
			'custom_class' => ''
		), $atts));


		$spans = $columns;

		// columns
		switch ($spans) {
			case '1':
				$spans = 'span12';
				break;
			case '2':
				$spans = 'span6';
				break;
			case '3':
				$spans = 'span4';
				break;
			case '4':
				$spans = 'span3';
				break;
			case '6':
				$spans = 'span2';
				break;
		}

		// check what order by method user selected
		switch ($order_by) {
			case 'date':
				$order_by = 'post_date';
				break;
			case 'title':
				$order_by = 'title';
				break;
			case 'popular':
				$order_by = 'comment_count';
				break;
			case 'random':
				$order_by = 'rand';
				break;
		}

		// check what order method user selected (DESC or ASC)
		switch ($order) {
			case 'DESC':
				$order = 'DESC';
				break;
			case 'ASC':
				$order = 'ASC';
				break;
		}

		// show link after posts?
		switch ($link) {
			case 'yes':
				$link = true;
				break;
			case 'no':
				$link = false;
				break;
		}

			global $post;
			global $my_string_limit_words;

			$numb = $columns * $rows;
							
			$args = array(
				'post_type' => $type,
				'numberposts' => $numb,
				'orderby' => $order_by,
				'order' => $order
			);		

			$posts = get_posts($args);
			$i = 0;
			$count = 1;
			$output_end = '';
			if ($numb > count($posts)) {
				$output_end = '</ul>';
			}

			$output = '<ul class="posts-grid row-fluid unstyled '. $custom_class .'">';

			for ( $j=0; $j < count($posts); $j++ ) {
				$post_id = $posts[$j]->ID;
				setup_postdata($posts[$j]);
				$excerpt = get_the_excerpt();
				$attachment_url = wp_get_attachment_image_src( get_post_thumbnail_id($post_id), 'full' );
				$url = $attachment_url['0'];
				$image = aq_resize($url, $thumb_width, $thumb_height, true);
				$mediaType = get_post_meta($post_id, 'tz_portfolio_type', true);
				$prettyType = 0;

					if ($count > $columns) {
						$count = 1;
						$output .= '<ul class="posts-grid row-fluid unstyled '. $custom_class .'">';
					}

					$output .= '<li class="'. $spans .'">';
						if(has_post_thumbnail($post_id) && $mediaType == 'Image') {
												
							$prettyType = 'prettyPhoto';									

							$output .= '<figure class="featured-thumbnail thumbnail">';
							$output .= '<a href="'.$url.'" title="'.get_the_title($post_id).'" rel="' .$prettyType.'">';
							$output .= '<img  src="'.$image.'" alt="'.get_the_title($post_id).'" />';
							$output .= '<span class="zoom-icon"><span class="zoom-text">'.__('zoom', CURRENT_THEME).'</span></span></a></figure>';
						} elseif ($mediaType != 'Video' && $mediaType != 'Audio') {					

							$thumbid = 0;
							$thumbid = get_post_thumbnail_id($post_id);
											
							$images = get_children( array(
								'orderby' => 'menu_order',
								'order' => 'ASC',
								'post_type' => 'attachment',
								'post_parent' => $post_id,
								'post_mime_type' => 'image',
								'post_status' => null,
								'numberposts' => -1
							) ); 

							if ( $images ) {

								$k = 0;
								//looping through the images
								foreach ( $images as $attachment_id => $attachment ) {
									$prettyType = "prettyPhoto[gallery".$i."]";								
									//if( $attachment->ID == $thumbid ) continue;

									$image_attributes = wp_get_attachment_image_src( $attachment_id, 'full' ); // returns an array
									$img = aq_resize( $image_attributes[0], $thumb_width, $thumb_height, true ); //resize & crop img
									$alt = get_post_meta($attachment->ID, '_wp_attachment_image_alt', true);
									$image_title = $attachment->post_title;

									if ( $k == 0 ) {
										if (has_post_thumbnail($post_id)) {
											$output .= '<figure class="featured-thumbnail thumbnail">';
											$output .= '<a href="'.$image_attributes[0].'" title="'.get_the_title($post_id).'" rel="' .$prettyType.'">';
											$output .= '<img src="'.$image.'" alt="'.get_the_title($post_id).'" />';
										} else {
											$output .= '<figure class="featured-thumbnail thumbnail">';
											$output .= '<a href="'.$image_attributes[0].'" title="'.get_the_title($post_id).'" rel="' .$prettyType.'">';
											$output .= '<img  src="'.$img.'" alt="'.get_the_title($post_id).'" />';
										}
									} else {
										$output .= '<figure class="featured-thumbnail thumbnail" style="display:none;">';
										$output .= '<a href="'.$image_attributes[0].'" title="'.get_the_title($post_id).'" rel="' .$prettyType.'">';
										$output .= '<img  src="'.$img.'" alt="'.get_the_title($post_id).'" />';
									}
									$output .= '<span class="zoom-icon"><span class="zoom-text">'.__('zoom', CURRENT_THEME).'</span></span></a></figure>';
									$k++;
								}					
							} elseif (has_post_thumbnail($post_id)) {
								$prettyType = 'prettyPhoto';
								$output .= '<figure class="featured-thumbnail thumbnail">';
								$output .= '<a href="'.$url.'" title="'.get_the_title($post_id).'" rel="' .$prettyType.'">';
								$output .= '<img  src="'.$image.'" alt="'.get_the_title($post_id).'" />';
								$output .= '<span class="zoom-icon"><span class="zoom-text">'.__('zoom', CURRENT_THEME).'</span></span></a></figure>';
							}
						} else {

							// for Video and Audio post format - no lightbox
							$output .= '<figure class="featured-thumbnail thumbnail"><a href="'.get_permalink($post_id).'" title="'.get_the_title($post_id).'">';
							$output .= '<img  src="'.$image.'" alt="'.get_the_title($post_id).'" />';
							$output .= '</a></figure>';
						}

						$output .= '<div class="clear"></div>';

						$output .= '<h5><a href="'.get_permalink($post_id).'" title="'.get_the_title($post_id).'">';
							$output .= get_the_title($post_id);
						$output .= '</a></h5>';

						if ($meta == 'yes') {
							// begin post meta
							$output .= '<div class="post_meta">';

								// post category
								$output .= '<span class="post_category">';
								if ($type!='' && $type!='post') {
									$terms = get_the_terms( $post_id, $type.'_category');
									if ( $terms && ! is_wp_error( $terms ) ) {
										$out = array();
										$output .= '<em>Posted in </em>';
										foreach ( $terms as $term )
											$out[] = '<a href="' .get_term_link($term->slug, $type.'_category') .'">'.$term->name.'</a>';
											$output .= join( ', ', $out );
									}
								} else {
									$categories = get_the_category();
									if($categories){
										$out = array();
										$output .= '<em>Posted in </em>';
										foreach($categories as $category)
											$out[] = '<a href="'.get_category_link($category->term_id ).'" title="'.$category->name.'">'.$category->cat_name.'</a> ';
											$output .= join( ', ', $out );
									}
								}
								$output .= '</span>';

								// post date
								$output .= '<span class="post_date">';
								$output .= '<time datetime="'.get_the_time('Y-m-d\TH:i:s', $post_id).'">' .get_the_time( get_option( 'date_format' ), $post_id ). '</time>';
								$output .= '</span>';

								// post author
								$output .= '<span class="post_author">';
								$output .= '<em>by </em>';
								$output .= '<a href="'.get_author_posts_url(get_the_author_meta( 'ID' )).'">'.get_the_author_meta('display_name').'</a>';
								$output .= '</span>';

								// post comment count
								$num = 0;						
								$queried_post = get_post($post_id);
								$cc = $queried_post->comment_count;
								if( $cc == $num || $cc > 1 ) : $cc = $cc.' Comments';
								else : $cc = $cc.' Comment';
								endif;
								$permalink = get_permalink($post_id);
								$output .= '<span class="post_comment">';
								$output .= '<a href="'. $permalink . '" class="comments_link">' . $cc . '</a>';
								$output .= '</span>';

							$output .= '</div>';
							// end post meta
						}

						if($excerpt_count >= 1){
							$output .= '<p class="excerpt">';
								$output .= my_string_limit_words($excerpt,$excerpt_count);
							$output .= '</p>';
						}
						if($link){
							$output .= '<a href="'.get_permalink($post_id).'" class="btn btn-primary" title="'.get_the_title($post_id).'">';
							$output .= $link_text;
							$output .= '</a>';
						}
						$output .= '</li>';
						if ($j == count($posts)-1) {
							$output .= $output_end;
						}
					if ($count % $columns == 0) {
						$output .= '</ul><!-- .posts-grid (end) -->';
					}
				$count++;
				$i++;		

			} // end for
			
			return $output;
	}	 
	add_shortcode('posts_grid', 'posts_grid_shortcode');
	
}

//Recent Posts
if (!function_exists('shortcode_recent_posts')) {

	function shortcode_recent_posts($atts, $content = null) {		
		extract(shortcode_atts(array(
				'type' => 'post',											 
				'category' => '',
				'custom_category' => '',
				'post_format' => 'standard',
				'num' => '5',
				'meta' => 'true',
				'thumb' => 'true',
				'thumb_width' => '120',
				'thumb_height' => '120',
				'more_text_single' => '',
				'excerpt_count' => '0',
				'custom_class' => ''
		), $atts));

		$output = '<ul class="recent-posts '.$custom_class.' unstyled">';

		global $post;
		global $my_string_limit_words;
		
		if($post_format == 'standard') {
						
			$args = array(
						'post_type' => $type,
						'category_name' => $category,
						$type . '_category' => $custom_category,
						'numberposts' => $num,
						'orderby' => 'post_date',
						'order' => 'DESC',
						'tax_query' => array(
						 'relation' => 'AND',
							array(
								'taxonomy' => 'post_format',
								'field' => 'slug',
								'terms' => array('post-format-aside', 'post-format-gallery', 'post-format-link', 'post-format-image', 'post-format-quote', 'post-format-audio', 'post-format-video'),
								'operator' => 'NOT IN'
							)
						)
						);
		
		} else {
		
			$args = array(
				'post_type' => $type,
				'category_name' => $category,
				$type . '_category' => $custom_category,
				'numberposts' => $num,
				'orderby' => 'post_date',
				'order' => 'DESC',
				'tax_query' => array(
				 'relation' => 'AND',
					array(
						'taxonomy' => 'post_format',
						'field' => 'slug',
						'terms' => array('post-format-' . $post_format)
					)
				)
				);
		
		}

		$latest = get_posts($args);
		
		foreach($latest as $post) {
				setup_postdata($post);
				$excerpt = get_the_excerpt();
				$attachment_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
				$url = $attachment_url['0'];
				$image = aq_resize($url, $thumb_width, $thumb_height, true);
				
				
				$post_classes = get_post_class();
				foreach ($post_classes as $key => $value) {
					$pos = strripos($value, 'tag-');
					if ($pos !== false) {
						unset($post_classes[$key]);
					}
				}
				$post_classes= implode(' ', $post_classes);
				

				$output .= '<li class="recent-posts_li ' . $post_classes . '">';
				
				
				//Aside
				if($post_format == "aside") {
					
					$output .= the_content($post->ID);
				
				} elseif ($post_format == "link") {
				
					$url =  get_post_meta(get_the_ID(), 'tz_link_url', true);
				
					$output .= '<a target="_blank" href="'. $url . '">';
					$output .= get_the_title($post->ID);
					$output .= '</a>';
				
				
				//Quote
				} elseif ($post_format == "quote") {
				
					$quote =  get_post_meta(get_the_ID(), 'tz_quote', true);
					
					$output .= '<div class="quote-wrap clearfix">';
							
							$output .= '<blockquote>';
								$output .= $quote;
							$output .= '</blockquote>';
							
					$output .= '</div>';
					
				
				//Image
				} elseif ($post_format == "image") {
				
				if (has_post_thumbnail() ):
				
				$lightbox = get_post_meta(get_the_ID(), 'tz_image_lightbox', TRUE);
				
				$src = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), array( '9999','9999' ), false, '' );
				
				$thumb = get_post_thumbnail_id();
				$img_url = wp_get_attachment_url( $thumb,'full'); //get img URL
				$image = aq_resize( $img_url, 200, 120, true ); //resize & crop img
				
				
				$output .= '<figure class="thumbnail featured-thumbnail large">';
					$output .= '<a class="image-wrap" rel="prettyPhoto[gallery]" title="' . get_the_title($post->ID) . '" href="' . $src[0] . '">';
					$output .= '<img src="' . $image . '" alt="' . get_the_title($post->ID) .'" />';
					$output .= '<span class="zoom-icon"><span class="zoom-text">'.__('zoom', CURRENT_THEME).'</span></span></a>';
				$output .= '</figure>';
				
				endif;
				
				
				//Audio
				} elseif ($post_format == "audio") {
				
					$template_url = get_template_directory_uri();
					$id = $post->ID;
				
					// get audio attribute
					$audio_title = get_post_meta(get_the_ID(), 'tz_audio_title', true);
					$audio_artist = get_post_meta(get_the_ID(), 'tz_audio_artist', true);		
					$audio_format = get_post_meta(get_the_ID(), 'tz_audio_format', true);
					$audio_url = get_post_meta(get_the_ID(), 'tz_audio_url', true);
						
					$output .= '<script type="text/javascript">
						$(document).ready(function(){
							var myPlaylist_'. $id.'  = new jPlayerPlaylist({
							jPlayer: "#jquery_jplayer_'. $id .'",
							cssSelectorAncestor: "#jp_container_'. $id .'"
							}, [
							{
								title:"'. $audio_title .'",
								artist:"'. $audio_artist .'",
								'. $audio_format .' : "'. stripslashes(htmlspecialchars_decode($audio_url)) .'"}
							], { 
								playlistOptions: {enableRemoveControls: false},
								ready: function () {$(this).jPlayer("setMedia", {'. $audio_format .' : "'. stripslashes(htmlspecialchars_decode($audio_url)) .'", poster: "'. $image .'"});
							},
							swfPath: "'. $template_url .'/flash",
							supplied: "'. $audio_format .', all",
							wmode:"window"
							});
						});
						</script>';
						
					$output .= '<div id="jquery_jplayer_'. $id .'" class="jp-jplayer"></div>';
					$output .= '<div id="jp_container_'. $id .'" class="jp-audio">';
					$output .= '<div class="jp-type-single">';
					$output .= '<div class="jp-gui">';
					$output .= '<div class="jp-interface">';
					$output .= '<div class="jp-progress">';
					$output .= '<div class="jp-seek-bar"><div class="jp-play-bar"></div></div>';
					$output .= '</div>';
					$output .= '<div class="jp-duration"></div><div class="jp-time-sep"></div><div class="jp-current-time"></div>';
					$output .= '<div class="jp-controls-holder">';
					$output .= '<ul class="jp-controls">';
					$output .= '<li><a href="javascript:;" class="jp-play" tabindex="1" title="Play"><span>Play</span></a></li>';
					$output .= '<li><a href="javascript:;" class="jp-pause" tabindex="1" title="Pause"><span>Pause</span></a></li>';
					$output .= '<li><a href="javascript:;" class="jp-stop" tabindex="1" title="Stop"><span>Stop</span></a></li>';
					$output .= '</ul>';
					$output .= '<div class="jp-volume-bar">';
					$output .= '<div class="jp-volume-bar-value">';
					$output .= '</div></div>';
					$output .= '<ul class="jp-toggles">';
					$output .= '<li><a href="javascript:;" class="jp-mute" tabindex="1" title="Mute"><span>Mute</span></a></li>';
					$output .= '<li><a href="javascript:;" class="jp-unmute" tabindex="1" title="Unmute"><span>Unmute</span></a></li>';
					$output .= '</ul>';
					$output .= '</div>';
					$output .= '<div class="jp-no-solution">';
					$output .= '<span>Update Required</span>';
					$output .= 'To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.';
					$output .= '</div></div></div>';
					$output .= '<div class="jp-playlist"><ul><li></li></ul></div>';
					$output .= '</div>';
				
				
				$output .= '<div class="entry-content">';
					$output .= get_the_content($post->ID);
				$output .= '</div>';
				
				//Video
				} elseif ($post_format == "video") {
					
					$template_url = get_template_directory_uri();
					$id = $post->ID;
				
					// get video attribute
					$video_title = get_post_meta(get_the_ID(), 'tz_video_title', true);
					$video_artist = get_post_meta(get_the_ID(), 'tz_video_artist', true);
					$embed = get_post_meta(get_the_ID(), 'tz_video_embed', true);
					$m4v_url = get_post_meta(get_the_ID(), 'tz_m4v_url', true);
					$ogv_url = get_post_meta(get_the_ID(), 'tz_ogv_url', true);
					
					// get thumb
					if(has_post_thumbnail()) {
						$thumb = get_post_thumbnail_id();
						$img_url = wp_get_attachment_url( $thumb,'full'); //get img URL
						$image = aq_resize( $img_url, 770, 380, true ); //resize & crop img
					}

					if ($embed == '') {
						$output .= '<script type="text/javascript">
							$(document).ready(function(){
								var myPlaylist_'. $id.'  = new jPlayerPlaylist({
								jPlayer: "#jquery_jplayer_'. $id .'",
								cssSelectorAncestor: "#jp_container_'. $id .'"
								}, [
								{
									title: "'. $video_title .'",
									artist: "'. $video_artist .'",
									mv4: "'. stripslashes(htmlspecialchars_decode($m4v_url)) .'",
									ogv: "'. stripslashes(htmlspecialchars_decode($ogv_url)) .'",
									poster: "'. $image .'"
								}
								], { 
									playlistOptions: {enableRemoveControls: false},
									ready: function () {$(this).jPlayer("setMedia", {m4v : "'. stripslashes(htmlspecialchars_decode($m4v_url)) .'", ogv : "'. stripslashes(htmlspecialchars_decode($ogv_url)) .'",});
								},
								swfPath: "'. $template_url .'/flash",
								supplied: "mv4, ogv, all",
								wmode:"window",
								size: {width: "100%", height: "100%"}
								});
							});
							</script>';
							$output .= '<div id="jp_container_'. $id .'" class="jp-video fullwidth playlist">';
							$output .= '<div class="jp-type-list-parent">';
							$output .= '<div class="jp-type-single">';
							$output .= '<div id="jquery_jplayer_'. $id .'" class="jp-jplayer"></div>';
							$output .= '<div class="jp-gui">';
							$output .= '<div class="jp-video-play">';
							$output .= '<a href="javascript:;" class="jp-video-play-icon" tabindex="1" title="Play">Play</a></div>';
							$output .= '<div class="jp-interface">';
							$output .= '<div class="jp-progress">';
							$output .= '<div class="jp-seek-bar">';
							$output .= '<div class="jp-play-bar">';
							$output .= '</div></div></div>';
							$output .= '<div class="jp-duration"></div>';
							$output .= '<div class="jp-time-sep">/</div>';
							$output .= '<div class="jp-current-time"></div>';
							$output .= '<div class="jp-controls-holder">';
							$output .= '<ul class="jp-controls">';
							$output .= '<li><a href="javascript:;" class="jp-play" tabindex="1" title="Play"><span>Play</span></a></li>';
							$output .= '<li><a href="javascript:;" class="jp-pause" tabindex="1" title="Pause"><span>Pause</span></a></li>';
							$output .= '<li class="li-jp-stop"><a href="javascript:;" class="jp-stop" tabindex="1" title="Stop"><span>Stop</span></a></li>';
							$output .= '</ul>';
							$output .= '<div class="jp-volume-bar">';
							$output .= '<div class="jp-volume-bar-value">';
							$output .= '</div></div>';
							$output .= '<ul class="jp-toggles">';
							$output .= '<li><a href="javascript:;" class="jp-mute" tabindex="1" title="Mute"><span>Mute</span></a></li>';
							$output .= '<li><a href="javascript:;" class="jp-unmute" tabindex="1" title="Unmute"><span>Unmute</span></a></li>';
							$output .= '</ul>';
							$output .= '</div></div>';
							$output .= '<div class="jp-no-solution">';
							$output .= '<span>Update Required</span>';
							$output .= 'To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.';
							$output .= '</div></div></div></div>';
							$output .= '<div class="jp-playlist"><ul><li></li></ul></div>';
							$output .= '</div>';
					}
					
					if($excerpt_count >= 1){
						$output .= '<div class="excerpt">';
							$output .= my_string_limit_words($excerpt,$excerpt_count);
						$output .= '</div>';
				}
				
				//Standard
				} else {
				
				if ($thumb == 'true') {
						if ( has_post_thumbnail($post->ID) ){
								$output .= '<figure class="thumbnail featured-thumbnail"><a href="'.get_permalink($post->ID).'" title="'.get_the_title($post->ID).'">';
								$output .= '<img  src="'.$image.'"/>';
								$output .= '</a></figure>';
						}
					}
							$output .= '<h5><a href="'.get_permalink($post->ID).'" title="'.get_the_title($post->ID).'">';
									$output .= get_the_title($post->ID);
							$output .= '</a></h5>';
							if($meta == 'true'){
									$output .= '<span class="meta">';
											$output .= '<span class="post-date">';
												$output .= get_the_time( get_option( 'date_format' ) );
											$output .= '</span>';
											$output .= '<span class="post-comments">';
												$output .= '<a href="'.get_comments_link($post->ID).'">';
													$output .= get_comments_number($post->ID);
												$output .= '</a>';
											$output .= '</span>';
									$output .= '</span>';
							}
					if($excerpt_count >= 1){
						$output .= '<div class="excerpt">';
							$output .= my_string_limit_words($excerpt,$excerpt_count);
						$output .= '</div>';
					}
					if($more_text_single!=""){
						$output .= '<a href="'.get_permalink($post->ID).'" class="btn btn-primary" title="'.get_the_title($post->ID).'">';
						$output .= $more_text_single;
						$output .= '</a>';
					}
				
				}				
			$output .= '<div class="clear"></div>';
			$output .= '</li><!-- .entry (end) -->';
		}
		$output .= '</ul><!-- .recent-posts (end) -->';
		return $output;		
	}
	add_shortcode('recent_posts', 'shortcode_recent_posts');

}

/*-----------------------------------------------------------------------------------*/
/* Output image */
/*-----------------------------------------------------------------------------------*/

if ( !function_exists( 'tz_image' ) ) {
    function tz_image($postid, $imagesize) {
		if (has_post_thumbnail($postid) ):
			$lightbox = get_post_meta(get_the_ID(), 'tz_image_lightbox', TRUE);
			if($lightbox == 'yes')
				$lightbox = TRUE;
			else
				$lightbox = FALSE;
			$src = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), array( '9999','9999' ), false, '' );

	        // get the featured image for the post
	        if( has_post_thumbnail($postid) ) {
				$thumb = get_post_thumbnail_id();
				$img_url = wp_get_attachment_url( $thumb,'full'); //get img URL
				$image = aq_resize( $img_url, 700, 460, true ); //resize & crop img

				if($lightbox) :
					echo '<figure class="featured-thumbnail thumbnail large"><a class="image-wrap" rel="prettyPhoto" title="'. get_the_title() .'" href="'. $src[0] .'"><img src="'. $image .'" alt="'. get_the_title() .'" /><span class="zoom-icon"><span class="zoom-text">'.__('zoom', CURRENT_THEME).'</span></span></a></figure><div class="clear"></div>';
				else :
					echo '<figure class="featured-thumbnail thumbnail large"><img src="'. $image .'" alt="'. get_the_title() .'" /></figure><div class="clear"></div>';
				endif;						
        	}
        endif;
    }
}

/*-----------------------------------------------------------------------------------*/
/* Output gallery */
/*-----------------------------------------------------------------------------------*/

if ( !function_exists( 'tz_grid_gallery' ) ) {
    function tz_grid_gallery($postid, $imagesize) { ?>
				
				<!--BEGIN .slider -->
					<div class="grid_gallery clearfix">
					
						<div class="grid_gallery_inner">
							
						<?php 
								$args = array(
										'orderby'		 => 'menu_order',
										'order' => 'ASC',
										'post_type'      => 'attachment',
										'post_parent'    => get_the_ID(),
										'post_mime_type' => 'image',
										'post_status'    => null,
										'numberposts'    => -1,
								);
								$attachments = get_posts($args);
						?>
								
								<?php if ($attachments) : ?>
								
								<?php foreach ($attachments as $attachment) : ?>
										
										<?php 
											$attachment_url = wp_get_attachment_image_src( $attachment->ID, 'full' );
											$url = $attachment_url['0'];
											$image = aq_resize($url, 260, 160, true);
										?>
										
										<div class="gallery_item">
											<figure class="featured-thumbnail single-gallery-item">
												<a href="<?php echo $attachment_url['0'] ?>" class="image-wrap" rel="prettyPhoto[gallery]">
												<img 
												alt="<?php echo apply_filters('the_title', $attachment->post_title); ?>"
												src="<?php echo $image ?>"
												width="260"
												height="160"
												/>
												<span class="zoom-icon"><span class="zoom-text"><?php _e('zoom', CURRENT_THEME); ?></span></span>
												</a>
											</figure>
										</div>
								
								<?php endforeach; ?>
								
								<?php endif; ?>
							
						</div>

					<!--END .slider -->
					</div>
				
				
        
    <?php }
}
	
?>