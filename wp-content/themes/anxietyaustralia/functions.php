<?php
    define('assets_css','1.4');
    define('assets_js','1.4');
?>
<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/customizer.php', // Theme customizer
  'lib/class-wp-bootstrap-navwalker.php', // Navwalker
  'lib/breadcrumbs-code.php', // Navwalker
  'lib/quicktags.php' // Navwalker
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);

if( function_exists('acf_add_options_page') ) {
    acf_add_options_page('Theme Settings');
}


add_filter( 'wp_nav_menu_items', 'your_custom_menu_item', 10, 2 );
function your_custom_menu_item ( $items, $args ) {
    if ($args->theme_location == 'secondary_navigation') {

        $secondary_menu = get_field('secondary_menu','option');
        if ($links = $secondary_menu['links']) {
            foreach ($links as $link){
                $items = '<li class="nav-item">'.$link['link'].'</li>' . $items;
            }
        }
    }
    return $items;
}

/*
Sample...  Lorem ipsum habitant morbi (26 characters total)

Returns first three words which is exactly 21 characters including spaces
Example..  echo get_excerpt(21);
Result...  Lorem ipsum habitant

Returns same as above, not enough characters in limit to return last word
Example..  echo get_excerpt(24);
Result...  Lorem ipsum habitant

Returns all 26 chars of our content, 30 char limit given, only 26 characters needed.
Example..  echo get_excerpt(30);
Result...  Lorem ipsum habitant morbi
*/
function get_excerpt($limit, $source = null){

    $excerpt = $source == "content" ? get_the_content() : get_the_excerpt();
    $excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
    $excerpt = strip_shortcodes($excerpt);
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, $limit);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
    $excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
    $excerpt = $excerpt.'...';
    return $excerpt;
}


//Add Upload Logo
add_theme_support( 'custom-logo' );
function themename_custom_logo_setup() {
    $defaults = array(
        'width'       => 250,
        'height'      => 75,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
    );
    add_theme_support( 'custom-logo', $defaults );
}
add_action( 'after_setup_theme', 'themename_custom_logo_setup' );