<?php
// Add buttons to html editor
add_action('admin_print_footer_scripts','rc_quicktags');
function rc_quicktags() { ?>
<script language="javascript" type="text/javascript">
    /* Adding Quicktag buttons to the editor WordPress ver. 3.3 and above
     * - Button HTML ID (required)
     * - Button display, value="" attribute (required)
     * - Opening Tag (required)
     * - Closing Tag (required)
     * - Access key, accesskey="" attribute for the button (optional)
     * - Title, title="" attribute (optional)
     * - Priority/position on bar, 1-9 = first, 11-19 = second, 21-29 = third, etc. (optional)
     */
    QTags.addButton( 'OwlButton', 'Owl Button Container', '<div class="owl-action">', '</div>', 'w' );
    QTags.addButton( 'span', 'Span', '<span class="span">', '</span>', 'w' );
    QTags.addButton( 'ButtonWarning', 'Button Warning', '<a href="#" class="btn btn-warning">', '</a>', 'w' );
    QTags.addButton( 'ButtonPrimary', 'Button primary', '<a href="#" class="btn btn-primary">', '</a>', 'w' );
    QTags.addButton( 'ButtonOutlinePrimary', 'Button Outline primary', '<a href="#" class="btn btn-outline-primary">', '</a>', 'w' );
    QTags.addButton( 'BtnLink', 'Button Link', '<a href="#" class="btn btn-link">', '</a>', 'w' );
    QTags.addButton( 'paragraph', 'p', '<p class="p">', '</p>', 'w' );
    QTags.addButton( 'SpanMeta', 'SpanMeta', '<span class="meta">', '</span>', 'w' );
    QTags.addButton( 'Facebook', 'Facebook', '<a href=""><i class="icon-facebook-letter-logo">', '</a></i>', 'w' );
    QTags.addButton( 'Twitter', 'Twitter', '<a href=""><i class="icon-twitter-logo-silhouette">', '</a></i>', 'w' );
    QTags.addButton( 'Div', 'Div', '<div>', '</div>', 'w' );
    QTags.addButton( 'BtnContainer', 'Btn Container', '<div class="btn-container">', '</div>', 'w' );
</script>
<?php
}