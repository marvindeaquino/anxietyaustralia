<?php

if( have_rows('global_sections') ):

    while ( have_rows('global_sections') ) : the_row();

        if( get_row_layout() == 'stop_avoiding' ):

            get_template_part('templates-extra/flex', 'stop-avoiding');

        endif;

    endwhile;

endif;
