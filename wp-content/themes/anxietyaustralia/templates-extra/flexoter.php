<?php $archive = get_field('archive','option'); ?>
<?php if ($types = $archive['archive_type']):?>
    <?php foreach ($types as $type): ?>
        <?php if (get_post_type() == $type['slug']):?>
            <?php if ($footer_global_sections = $type['footer_global_sections']): ?>
                <?php foreach ($footer_global_sections as $footer_global_section): ?>
                    <?php

                    if( $footer_global_section['acf_fc_layout'] == 'stop_avoiding' ):
                        get_template_part('templates-extra/flex', 'stop-avoiding');

                    elseif( $footer_global_section['acf_fc_layout'] == 'connect_with_us' ):
                        get_template_part('templates-extra/flex', 'connect');

                    elseif( $footer_global_section['acf_fc_layout'] == 'good_reasons' ):
                        get_template_part('templates-extra/flex', 'good-reasons');

                    endif; ?>
                <?php endforeach;?>
            <?php endif;?>
        <?php endif;?>
    <?php endforeach; ?>
<?php endif;?>