<?php $good_reasons = get_field('good_reasons','option'); ?>
<div class="section-good-reasons">
    <div class="container">
        <h2 class="section-title"><?php echo $good_reasons['title']; ?></h2>
        <?php if ($items = $good_reasons['item']): ?>
            <div class="row">
                <?php foreach ($items as $item):?>
                    <div class="col-md-6 col-lg-3">
                        <div>
                        <span>
                            <img src="<?php echo $item['image']['url'] ?>" alt="<?php echo $item['image']['alt'] ?>">
                        </span>
                            <?php echo $item['content']; ?>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
        <?php endif;?>
    </div>
</div>