<?php $what_we_do = get_sub_field('what_we_do'); ?>
<div class="section-what-we-do">
    <div class="container">
        <div class="content">
            <?php echo $what_we_do['Content']; ?>
        </div>
    </div>
</div>

