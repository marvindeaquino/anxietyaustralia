<?php while (have_posts()) : the_post(); ?>
    <?php get_template_part('templates/page', 'header'); ?>
    <div class="section-with-sidebar">
        <div class="container">
            <div class="main">
                <?php the_content(); ?>
                <?php get_template_part('templates/flex', ''); ?>
            </div>
            <?php $sidebar = get_field('sidebar','option') ?>
            <?php if ($widgets = $sidebar['widgets']): ?>
                <div class="sidebar">
                    <?php foreach ($widgets as $widget): ?>
                        <div class="<?php echo $widget['style']['value']?>" <?php echo !empty($widget['image']['url'])? 'style="background-image:url('. $widget['image']['url'].');"':'';?>>
                            <?php echo $widget['widget']; ?>
                        </div>
                    <?php endforeach;?>
                </div>
            <?php endif;?>
        </div>
    </div>
    <?php include locate_template('templates-extra/flexoter.php'); ?>
<?php endwhile; ?>

