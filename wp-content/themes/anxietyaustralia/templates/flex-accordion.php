<?php $accordion = get_sub_field('accordion'); ?>
<?php if($items = $accordion['item']):?>
    <div class="accordion" id="accordionExample">
        <?php foreach ($items as $key=> $item):?>
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h2 data-toggle="collapse" data-target="#collapse<?php echo $key;?>" aria-expanded="true" aria-controls="collapse<?php echo $key;?>"><?php echo $item['title']; ?><i class="dropdown-toggle"></i></h2>
                </div>
                <div id="collapse<?php echo $key;?>" class="collapse <?php echo $key==0 ? 'show':''; ?>" aria-labelledby="heading<?php echo $key;?>" data-parent="#accordionExample">
                    <div class="card-body">
                        <?php echo $item['content']; ?>
                    </div>
                </div>
            </div>
        <?php endforeach;?>
    </div>
<?php endif;?>
