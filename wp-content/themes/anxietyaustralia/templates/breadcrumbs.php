<?php if(!empty(get_hansel_and_gretel_breadcrumbs())): ?>
    <div class="section-breadcrumbs">
        <div class="container">
            <?php echo get_hansel_and_gretel_breadcrumbs(); ?>
        </div>
    </div>
<?php endif; ?>