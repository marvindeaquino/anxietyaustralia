<?php $stop_avoiding = get_field('stop_avoiding','option'); ?>

<div class="section-stop-avoiding-anxiety bg-img bg-blend-primary" style="background-image: url('<?php echo get_template_directory_uri()?>/dist/images/home/meeting-with-client.jpg')">
    <div class="container">
        <?php echo $stop_avoiding['content']; ?>
    </div>
</div>