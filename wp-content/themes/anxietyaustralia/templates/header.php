<?php
/*if (has_nav_menu('primary_navigation')) :
    wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
endif;
*/?>

<nav class="navbar-main navbar navbar-expand-xl bg-primary">
    <!--<a class="navbar-brand" href="#">Expand at sm</a>-->
    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbar-main" aria-controls="navbar-main" aria-expanded="false" aria-label="Toggle navigation">
        <span class="hamburger-box">
            <span class="hamburger-inner"></span>
        </span>
    </button>
    <?php
    if (has_nav_menu('secondary_navigation')) :
        wp_nav_menu([
            'theme_location' => 'secondary_navigation',
            'container' => false,
            'depth' => 2,
            'menu_class' => 'navbar-nav-menu location-link-mobile',
            'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
            'walker' => new WP_Bootstrap_Navwalker()
        ]);
    endif;
    ?>
    <div class="collapse navbar-collapse" id="navbar-main">
        <?php
        if (has_nav_menu('primary_navigation')) :
            wp_nav_menu([
                'theme_location' => 'primary_navigation',
                'container' => false,
                'depth' => 3,
                'menu_class' => 'navbar-main-menu navbar-nav-menu navbar-nav m-auto',
                'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                'walker' => new WP_Bootstrap_Navwalker()
            ]);
        endif;
        ?>
    </div>

</nav>

<nav class="navbar-submain navbar navbar-expand-xl bg-white">
    <div class="container container-lg">
        <?php
        $custom_logo_id = get_theme_mod( 'custom_logo' );
        $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
        ?>
        <a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>"><img src="<?php echo $image[0]; ?>" alt=""></a>
        <?php
        if (has_nav_menu('secondary_navigation')) :
            wp_nav_menu([
                'theme_location' => 'secondary_navigation',
                'container' => false,
                'menu_class' => 'contact-number',
                'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',

            ]);
        endif;
        ?>
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbar-submain" aria-controls="navbar-submain" aria-expanded="false" aria-label="Toggle navigation">
             <span class="hamburger-box">
                <span class="hamburger-inner"></span>
             </span>
        </button>

        <div class="collapse navbar-collapse" id="navbar-submain">
            <?php
            if (has_nav_menu('secondary_navigation')) :
                wp_nav_menu([
                    'theme_location' => 'secondary_navigation',
                    'container' => false,
                    'depth' => 2,
                    'menu_class' => 'navbar-nav navbar-nav-menu',
                    'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                    'walker' => new WP_Bootstrap_Navwalker()
                ]);
            endif;
            ?>
        </div>
    </div>
</nav>













