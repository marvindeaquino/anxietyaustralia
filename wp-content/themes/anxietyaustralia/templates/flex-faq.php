<?php $faq = get_field('faq','option'); ?>
<div class="section-faq">
    <div class="container">
        <h2 class="section-title outline-bottom outline-white"><?php echo $faq['title']?></h2>
        <?php echo $faq['content']?>
    </div>
</div>