<?php

if( have_rows('default_template') ):

    while ( have_rows('default_template') ) : the_row();

        if( get_row_layout() == 'master_banner' ):

            get_template_part('templates/flex', 'master-banner');

        elseif( get_row_layout() == 'what_we_do' ):

            get_template_part('templates/flex', 'what-we-do');

        elseif( get_row_layout() == 'two_column' ):

            get_template_part('templates/flex', 'two-column');

        elseif( get_row_layout() == 'services' ):

            get_template_part('templates/flex', 'services');

        elseif( get_row_layout() == 'news' ):

            get_template_part('templates/flex', 'news');

        elseif( get_row_layout() == 'good_reasons' ):

            get_template_part('templates/flex', 'good-reasons');

        elseif( get_row_layout() == 'faq' ):

            get_template_part('templates/flex', 'faq');

        elseif( get_row_layout() == 'connect_with_us' ):

            get_template_part('templates/flex', 'connect');

        elseif( get_row_layout() == 'book_appointment' ):

            get_template_part('templates/flex', 'book-appointment');

        elseif( get_row_layout() == 'contact_details_map' ):

            get_template_part('templates/flex', 'contact-details-map');

        elseif( get_row_layout() == 'accordion' ):

            get_template_part('templates/flex', 'accordion');

        endif;

    endwhile;

endif;
