<div class="section-subpage-banner bg-img bg-blend-primary" style="background-image: url('<?php echo get_template_directory_uri()?>/dist/images/services/services-banner.jpg');">
    <div class="container">
        <h1><?php echo the_title(); ?></h1>
    </div>
</div>
