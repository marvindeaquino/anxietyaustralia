<?php $contact_details_map = get_sub_field('contact_details_map');?>

<div class="section-block section-with-map">
    <div class="container">
        <div class="website-details">
            <div class="contact-details">
                <div>
                    <?php if ($content_repeater = $contact_details_map['content_repeater']): ?>
                        <?php foreach ($content_repeater as $item): ?>
                            <div>
                                <?php echo $item['content']?>
                            </div>
                        <?php endforeach;?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="location-map">
                <div>
                    <?php echo $contact_details_map['map'];?>
                </div>
            </div>
        </div>
    </div>
</div>