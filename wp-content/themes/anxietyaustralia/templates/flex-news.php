<?php $news = get_sub_field('news');?>
<div class="section-latest-news">
    <div class="container">
        <h2 class="section-title outline-bottom outline-primary"><?php echo $news['title']; ?></h2>
        <div class="row">
            <?php
            $args = [
                'post_type' => 'post',
                'order' => 'DESC',
                'orderby' => 'date',
                'post_per_page' => 4
            ];
            // the query
            $the_query = new WP_Query( $args ); ?>

            <?php if ( $the_query->have_posts() ) : ?>

                <!-- pagination here -->

                <!-- the loop -->
                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <div class="col-lg-6 col-xl-3">
                        <div>
                            <span class="meta"><?php the_date()?></span>
                            <h3><?php the_title()?></h3>
                            <p><?php echo get_excerpt('117')?></p>
                            <a href="#" class="btn btn-link">Learn more</a>
                        </div>
                    </div>
                <?php endwhile; ?>
                <!-- end of the loop -->

                <!-- pagination here -->

                <?php wp_reset_postdata(); ?>

            <?php else : ?>
                <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>

        </div>
    </div>
</div>
