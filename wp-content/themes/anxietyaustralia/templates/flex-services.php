<?php $services = get_sub_field('services'); ?>

<div class="section-our-services">
    <div class="container">
        <h2 class="section-title outline-bottom outline-primary"><?php echo $services['title']; ?></h2>
        <?php if ($items = $services['item']): ?>
            <div class="row">
                <?php foreach ($items as $item):?>
                    <div class="col-md-6 col-lg-4">
                        <div>
                        <span class="animate-me">
                            <img src="<?php echo $item['image']['url']?>" alt="<?php echo $item['image']['alt']?>">
                        </span>
                            <?php echo $item['content']; ?>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
        <?php endif;?>
    </div>
</div>


