<?php $two_column = get_sub_field('two_column'); ?>
<?php if ($items = $two_column['item']): ?>
    <div class="section-after-what-we-do">
        <div class="container">
            <div class="row">
                <?php foreach ($items as $item):?>
                    <div class="col-md-12 col-lg-6">
                        <div style="background-image: url('<?php echo $item['background_image']['url']; ?>')">
                            <?php echo $item['content']; ?>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
        </div>
    </div>
<?php endif;?>
