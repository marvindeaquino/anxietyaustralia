<?php $book_appointment = get_sub_field('book_appointment'); ?>

<div class="section-block section-form-contact">
    <div class="container">
        <?php echo $book_appointment['content']; ?>
        <hr class="hr">
    </div>
</div>

