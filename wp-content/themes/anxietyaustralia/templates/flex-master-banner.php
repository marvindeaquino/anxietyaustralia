<?php $master_banner = get_sub_field('master_banner');?>
<?php if ($items = $master_banner['item']): ?>
    <div class="section-master-banner">
        <div class="owl-carousel owl-carousel-master owl-theme">
            <?php foreach ($items as $item): ?>
                <div style="background-image: url('<?php echo $item['background_image']['url']; ?>')">
                    <div class="container">
                        <?php echo $item['content']; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="owl-footer">
            <?php echo $master_banner['footer']?>
        </div>
    </div>
<?php endif;?>
