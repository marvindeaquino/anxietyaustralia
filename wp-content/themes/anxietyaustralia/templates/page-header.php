<?php $subpage_banner = get_field('subpage_banner') ?>
<?php $archive = get_field('archive','option'); ?>

<?php if ($types = $archive['archive_type']):?>
    <?php foreach ($types as $type): ?>
        <?php if (get_post_type() == $type['slug']):?>
            <?php if (is_home()):?>
                <div class="section-subpage-banner bg-img bg-blend-primary" style="background-image: url('<?php echo !empty($type['banner_image']['url']) ? $type['banner_image']['url'] : get_template_directory_uri()."/dist/images/services/services-banner.jpg"; ?>');">
                    <div class="container"><?php echo $type['content']; ?></div>
                </div>
            <?php endif;?>
        <?php endif;?>
    <?php endforeach; ?>
<?php endif;?>
<?php if (!is_home()):?>
    <div class="section-subpage-banner bg-img bg-blend-primary" style="background-image: url('<?php echo !empty($subpage_banner['image']['url']) ? $subpage_banner['image']['url'] : get_template_directory_uri()."/dist/images/services/services-banner.jpg"; ?>');">
        <div class="container">
            <?php if (is_404()): ?>
                <h1>404 <br>Page Not Found</h1>
                <p>The page you are looking for might have been removed<br>
                    had its name changed or is temporarily unvailable</p>
            <?php else: ?>
                <?php echo !empty($subpage_banner['title']) ? $subpage_banner['title']: '<h1>'.get_the_title().'</h1>'; ?>
            <?php endif;?>
        </div>
    </div>
<?php endif;?>



<?php if (!is_404()):?>
    <?php get_template_part('templates/breadcrumbs', ''); ?>
<?php endif;?>






