<?php $footer = get_field('footer','option'); ?>
<footer class="content-info">
    <div class="section-footer-links">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-3">
                    <div>
                        <a class="footer-logo" href="#"><img src="<?php echo $footer['logo']['url']?>" alt="<?php echo $footer['logo']['alt']; ?>"></a>
                    </div>
                </div>
                <?php if ($footer_links = $footer['footer_links']): ?>
                    <?php foreach ($footer_links as $footer_link): ?>
                        <div class="col-sm-12 col-md-3">
                            <div>
                                <h2><?php echo $footer_link['title']; ?></h2>
                                <?php if ($links = $footer_link['item']): ?>
                                    <ul>
                                        <?php foreach ($links as $link): ?>
                                            <li>
                                                <a href="<?php echo $link['link']['url']; ?>"><?php echo $link['link']['title']; ?></a>
                                            </li>
                                        <?php endforeach;?>
                                    </ul>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endforeach;?>
                <?php endif;?>

            </div>
        </div>
    </div>

    <div class="section-copyright">
        <div class="container">
            <?php echo $footer['copyright']; ?>
        </div>
    </div>
</footer>
