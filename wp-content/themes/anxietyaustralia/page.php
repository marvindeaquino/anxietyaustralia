<?php while (have_posts()) : the_post(); ?>
    <?php if (!is_front_page()): ?>
        <?php get_template_part('templates/page', 'header'); ?>
    <?php endif; ?>
    <div class="flex-wrap flex-wrap-page">
        <?php if (get_the_content()): ?>
            <div class="section section-block">
                <div class="container">
                    <?php the_content(); ?>
                </div>
            </div>
        <?php endif;?>
        <?php get_template_part('templates/flex','')?>
    </div>
<?php endwhile; ?>
